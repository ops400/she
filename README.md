# Simple Hex Editor
A port of the damm [imGuiTest](https://gitlab.com/ops400/misc/-/tree/main/imGuiTest) from [misc](https://gitlab.com/ops400/misc).
## Why
Well I need to later implement this to [COMP24 Emulator](https://gitlab.com/ops400/comp24/-/tree/main/c24emu), so I decided to make a portable version of it.
