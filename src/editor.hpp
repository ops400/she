#ifndef EDITOR_HPP_
#define EDITOR_HPP_
#include <cstdint>
#include <raylib.h>
#include "deps/imgui.h"

#define MAX_ELEMENTS 0x55
#define MIN_MINOR_BAR_HEIGHT 14.0f

void openEditor(uint8_t* data, ImGuiStyle* style, uint64_t fileSize, bool* firstRun, uint16_t* maxElements, bool debugInfo = false);

typedef struct compactRectangle{
    Rectangle rec;
    Color col;
} compactRectangle;

uint8_t coeficientCalcForJ(uint64_t i, uint64_t fileSize);
void iEnd_iStar_addvencer(uint64_t* iStart, uint64_t* iEnd, uint64_t fileSize, uint16_t maxElements);
void iEnd_iStar_reducer(uint64_t* iStart, uint64_t* iEnd, uint64_t fileSize, uint16_t maxElements);
void yLineatInterpolationForMinor(Rectangle* minor, Rectangle major, uint64_t iStart, uint64_t iEnd, uint64_t fileSize, uint16_t maxElements);
uint8_t addCoeficient(uint64_t fileSize);

const Color transparent = {0, 0, 0, 0};
const Color majorScrollBarColor = {18, 18, 18, 255};
const Color minorScrollBarColor = {79, 79, 79, 255};

const ImVec4 customWindowBg = {0.13f, 0.13f, 0.13f, 1.0f};
const ImVec4 disableText = {0.5f, 0.5f, 0.5f, 1.0f};
const ImVec4 whitePure = {1.0f, 1.0f, 1.0f, 1.0f};
const ImVec4 defaultFrameBg = {0.16f, 0.29f, 0.48f, 0.54f};
const ImVec4 modifiedFrameBg = {0.16f, 0.29f, 0.48f, 0.0f};
const ImVec4 customMenuBarBg = {0.204f, 0.204f, 0.204f, 1.0f};

#endif
