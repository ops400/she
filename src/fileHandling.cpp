#include "fileHandling.hpp"
#include <bits/types/FILE.h>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <string>

uint64_t getFileSize(char* fileName){
    uint64_t fileSize = 0;
    FILE* fp = fopen(fileName, "rb");
    if(fp == NULL){
        printf("ERROR!!!!!!\nAt getFileSize\nfp == NULL\n");
        exit(1);
    }
    fseek(fp, 0, SEEK_END);
    fileSize = ftell(fp);
    fclose(fp);

    return fileSize;
}

void openFile(uint8_t* data, char* fileName, uint64_t fileSize){
    uint8_t dataPrelude[fileSize];
    for(uint64_t i = 0; i < fileSize; i++) dataPrelude[i] = 0;
    FILE* fp = fopen(fileName, "rb");
    if(fp == NULL){
        printf("Error!\nAt openFile\n");
        exit(1);
    }
    fread(dataPrelude, 1, fileSize, fp);
    for(uint64_t i = 0; i < fileSize; i++) data[i] = dataPrelude[i];
    return;
}

void saveFile(uint8_t* data, char* fileName, uint64_t fileSize){
    FILE* fp = fopen(fileName, "wb");
    if(fp == NULL){
        printf("Error!\nAt saveFile\n");
        exit(1);
    }
    fwrite(data, sizeof(uint8_t), fileSize, fp);
    fclose(fp);

    return;
}
