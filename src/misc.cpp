#include "deps/imgui.h"
#include <cstdint>
#include <raylib.h>

ImVec2 calcP1(Rectangle rec){
    ImVec2 vec;
    vec.x = rec.x;
    vec.y = rec.y;
    return vec;
}

ImVec2 calcP2(Rectangle rec){
    ImVec2 vec;
    vec.x = rec.x + (rec.width-1);
    vec.y = rec.y + (rec.height-1);
    return vec;
}

ImU32 convColor(Color col){
    ImU32 colorFinal = 0;

    // my old solution didn't work
    // so I stole ImGui's
    colorFinal  = col.r << IM_COL32_R_SHIFT;
    colorFinal |= col.g << IM_COL32_G_SHIFT;
    colorFinal |= col.b << IM_COL32_B_SHIFT;
    colorFinal |= col.a << IM_COL32_A_SHIFT;

    return colorFinal;
}

ImVec4 convColorImVec4(Color col){
    ImVec4 finalColor;

    finalColor.w = (float)(col.a)/255;
    finalColor.x = (float)(col.r)/255;
    finalColor.y = (float)(col.g)/255;
    finalColor.z = (float)(col.b)/255;

    return finalColor;
}

uint8_t additionCoeficientIEnd(uint64_t fileSize, uint64_t currentIEnd){
    uint8_t res = 0;

    const int64_t sub = fileSize - currentIEnd;
    if(sub >= 5) res = 5;
    else if(sub < 5 && sub >= 0) res = sub;
    else res = 0;

    return res;
}
