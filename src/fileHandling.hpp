#ifndef FILEHANDLING_HPP_
#define FILEHANDLING_HPP_
#include <cstdint>
#include <string>

uint64_t getFileSize(char* fileName);
void openFile(uint8_t* data, char* fileName, uint64_t fileSize);
void saveFile(uint8_t* data, char* fileName, uint64_t fileSize); // do it later

#endif
