#ifndef MISC_HPP_
#define MISC_HPP_
#include "deps/imgui.h"
#include <cstdint>
#include <raylib.h>

ImVec2 calcP1(Rectangle rec);
ImVec2 calcP2(Rectangle rec);
ImU32 convColor(Color col);
ImVec4 convColorImVec4(Color col);

uint8_t additionCoeficientIEnd(uint64_t fileSize, uint64_t currentIEnd);

#endif
