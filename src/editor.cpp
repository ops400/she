#include "editor.hpp"
#include "misc.hpp"
#include "deps/imgui.h"
#include <bits/types/FILE.h>
#include <raylib.h>
#include <climits>
#include <cstdint>
#include <cstdio>

void openEditor(uint8_t* data, ImGuiStyle* style, uint64_t fileSize, bool* firstRun, uint16_t* maxElements, bool debugInfo){
    // ImDrawList* drawList = ImGui::GetWindowDrawList();

    static uint64_t iStart, iEnd, iDummyStart, iDummyEnd;
    static uint32_t jumpMultiplier;
    static bool isBiggerThan85;

    ImVec2 cursorPosLastFirstRowByte = {0.0f, 0.0f};
    float yCursorPosLosRowByte = 0.0f;

    static Rectangle majorScrollBarRec, mouseRefRec, minorScrollBarRec;
    // minorScrollBarRec
    // width : 9px
    // top-spacing: 3px

    // TEST VARIABLES!!!
        static float dummyWidith;
    // TEST VARIABLES!!!

    if(*firstRun == true){
        iStart = 0;
        if(fileSize > *maxElements)
            isBiggerThan85 = true;
        else
            isBiggerThan85 =  false;
        if(isBiggerThan85){
            iEnd = *maxElements;
        }
        else {
            iEnd = fileSize;
        }
        iDummyStart = iStart;
        iDummyEnd = iEnd;

        // can remove
            dummyWidith = 0.0f;
        // can remove

        minorScrollBarRec.width = 9.0f;
        majorScrollBarRec = {0.0f, 0.0f, 14, 0.0f}; // base 0x55 should be 388px long
        mouseRefRec = {(float)GetMouseX(), (float)GetMouseY(), 10, 10};

        jumpMultiplier = 1;

        *firstRun = false;
    }

    mouseRefRec.x = GetMouseX();
    mouseRefRec.y = GetMouseY();

    // "stylling" to make this look "similar" to ImHex
    ImGui::TextUnformatted("Adress"); ImGui::SameLine();
    ImGui::Dummy((ImVec2){2.5f, 0.0f}); ImGui::SameLine();
    // makes the 00 01 02 ..... on the top of the first row of values
    for(uint8_t k = 0; k < 5; k++){
        ImGui::Text("%02X", k);

        ImGui::SameLine();
        // strange that an 0 width thing makes the perfect spancing to line up
        ImGui::Dummy((ImVec2){0.0f, 0.0f});
        if(k < 4)
            ImGui::SameLine();
    }
    ImGui::SameLine(); ImGui::Dummy((ImVec2){1.0f, 0.0f});
    ImGui::SameLine(); ImGui::TextUnformatted("ASCII");

    // the "commander" steps though the whole data array
    for(uint64_t i = iStart; i < iEnd; i+=5){
        ImGui::Text("%06lX:", i);
        // ImGui::SameLine(); ImGui::Dummy((ImVec2){1.0f, 0.0f});
        ImGui::SameLine();



        // generates the inputs for the bytes
        for(uint64_t j = 0; j < coeficientCalcForJ(i, fileSize); j++){
            ImGui::SetNextItemWidth(21.0f);
            ImGui::InputScalar(TextFormat("##bit%lu", j+i), ImGuiDataType_U8, &data[j+i], NULL, NULL, "%02X");

            ImGui::SameLine();
            ImGui::Dummy((ImVec2){-7.0f, 0.0f});
            if(j < 4){
                ImGui::SameLine();
            }
            // else{
                // ImGui
            // }
        }

        // corrects spacing
        ImGui::SameLine();
        if(coeficientCalcForJ(i, fileSize) != 5){
            float spacing = 0.0f;
            switch(coeficientCalcForJ(i, fileSize)){
                case 1:
                    spacing = 108.0f;
                    break;
                case 2:
                    spacing = 79.0f;
                    break;
                case 3:
                    spacing = 50.0f;
                    break;
                case 4:
                    spacing = 21.0f;
                    break;

                default:
                    spacing = 0.0f;
            }
            // 1 108
            // 2 79
            // 3 50
            // 4 21
            ImGui::Dummy((ImVec2){spacing, 0.0f});
            ImGui::SameLine();
        }

        // makes that ascii fancy thing
        // you know like 4 f g . .
        for(uint64_t j = 0; j < coeficientCalcForJ(i, fileSize); j++){
            uint8_t currentByte = data[j+i];
            // cheks if currentByte is a valid ascii char
            if(currentByte < 0x21 || currentByte > 0x7F){
                ImGui::TextDisabled(".");
            }
            else
                ImGui::Text("%c", currentByte);
            if(j < (int8_t)coeficientCalcForJ(i, fileSize)-1){
                ImGui::SameLine();
            }
        }

        if(i == iStart && isBiggerThan85){
            ImGui::SameLine();
            cursorPosLastFirstRowByte = ImGui::GetCursorPos();
            ImGui::NewLine();
        }
        if(i == iEnd-coeficientCalcForJ(i, fileSize) && isBiggerThan85){
            ImGui::SameLine();
            yCursorPosLosRowByte = ImGui::GetCursorPosY();
            ImGui::NewLine();
        }
    }

    if(isBiggerThan85){
        majorScrollBarRec.x = cursorPosLastFirstRowByte.x;
        majorScrollBarRec.y = cursorPosLastFirstRowByte.y;

        // so the 20 is not all that a magic number
        // you see I saw that the input scalar is 19 pixels tall
        // and my calcP* removes 1 pixel
        // so 1+19 = 20
        // oh! And this calculates the height of the major scroll bar
        majorScrollBarRec.height = yCursorPosLosRowByte-majorScrollBarRec.y+20;

        if(CheckCollisionRecs(mouseRefRec, majorScrollBarRec)){
            if(GetMouseWheelMove() <= -1){
                iEnd_iStar_addvencer(&iStart, &iEnd, fileSize, *maxElements);
            }
            else if(GetMouseWheelMove() >= 1){
                iEnd_iStar_reducer(&iStart, &iEnd, fileSize, *maxElements);
            }
        }

        minorScrollBarRec.x = majorScrollBarRec.x+2.5;

        // idk if float by double can cause problems
        float maxElementsToFileSizeRatio = (float)*maxElements/(double)fileSize;
        // you see the maximum height of minor scroll bar is the actual height of major scroll bar, but subtracted by 6 ;)
        // because of the top and bottom 3px padding
        float maxHeightToActualRatio = (majorScrollBarRec.height-6)*maxElementsToFileSizeRatio;
        if(maxHeightToActualRatio < MIN_MINOR_BAR_HEIGHT)
            minorScrollBarRec.height = MIN_MINOR_BAR_HEIGHT;
        else
            minorScrollBarRec.height = maxHeightToActualRatio;

        // switch(iStart){
            // case 0:
                // minorScrollBarRec.y = majorScrollBarRec.y+3;
                // break;
            // default:
                yLineatInterpolationForMinor(&minorScrollBarRec, majorScrollBarRec, iStart, iEnd, fileSize, *maxElements);
        // }

        ImGui::Text("Current adress: %08lX ... %08lX", iStart, iEnd);
        // manualy increment the address by 5 or some other max value to not over load
        ImGui::SameLine();
        if(ImGui::Button("+")){
            for(uint32_t i = 0; i < jumpMultiplier; i++)
                iEnd_iStar_addvencer(&iStart, &iEnd, fileSize, *maxElements);
        } ImGui::SameLine();
        if(ImGui::Button("-")){
            for(uint32_t i = 0; i < jumpMultiplier; i++)
                iEnd_iStar_reducer(&iStart, &iEnd, fileSize, *maxElements);
        }
        ImGui::SetNextItemWidth(90.0f);
        ImGui::InputScalar("Advance multiplier", ImGuiDataType_U32, &jumpMultiplier);
        ImGui::SameLine(); ImGui::TextDisabled("(?)");
        if(ImGui::BeginItemTooltip()){
            ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);

            ImGui::TextUnformatted("Sets how many \"rows\" its going to advence. Like the standard 1 will progress trhough the address 5 by 5, but with this you can incress it");
            ImGui::TextUnformatted("If it's... lets say 3, it will advenc by 5 trhough the address 3 times.");
            ImGui::TextColored(convColorImVec4(YELLOW), "WARNIG: ");
            ImGui::SameLine(); ImGui::TextUnformatted("Really big values may slow down the software, but wont take long and it will be back to normal speed.");

            ImGui::PopTextWrapPos();
            ImGui::EndTooltip();
        }
        if(jumpMultiplier == 0)
            jumpMultiplier = 1;

        ImGui::GetWindowDrawList()->AddRectFilled(calcP1(majorScrollBarRec), calcP2(majorScrollBarRec), convColor(majorScrollBarColor));
        ImGui::GetWindowDrawList()->AddRectFilled(calcP1(minorScrollBarRec), calcP2(minorScrollBarRec), convColor(minorScrollBarColor));
    }

    if(debugInfo){
        ImGui::GetWindowDrawList()->AddRectFilled(calcP1(mouseRefRec), calcP2(mouseRefRec), convColor(ORANGE));
        if(ImGui::Begin("\"UnBugger\"")){

            ImGui::Text("FPS: %d", GetFPS());
            ImGui::Text("Rodinha do mouse: %f", GetMouseWheelMove());
            ImGui::Text("Coeficient: %u", addCoeficient(fileSize));
            ImGui::Text("iStart: %lu", iStart);
            ImGui::Text("iEnd: %lu", iEnd);
            ImGui::Text("iStart-iEnd: %ld", iEnd-iStart);
            ImGui::Text("additionCoeficientIEnd: %u", additionCoeficientIEnd(fileSize, iEnd));
            uint64_t iEndPer5 = iEnd%5;
            switch(iEndPer5){
                case 0:
                    iEndPer5 = 5;
                    break;
            }
            ImGui::Text("iEnd iStart sub main IF condition result: %u", (unsigned int)(!((int64_t)iStart-5 < 0) && (iEnd-iEndPer5)-(iStart-5) == *maxElements));
            ImGui::Text("iEnd%%5: %lu", iEnd%5);


            // can remove later
            ImGui::InputFloat("dummy width", &dummyWidith, 0.5f);
        }
        ImGui::End();
    }

    return;
}

void iEnd_iStar_addvencer(uint64_t* iStart, uint64_t* iEnd, uint64_t fileSize, uint16_t maxElements){
    if(((*iEnd+5) - (*iStart+5) == (maxElements) || ((*iEnd+5) - (*iStart+5) <= (maxElements) && (*iEnd+5) - (*iStart+5) >= (maxElements-5))) &&
    !(*iEnd+addCoeficient(fileSize) > fileSize)){
        *iStart += 5;

        if(*iEnd+5 > fileSize && addCoeficient(fileSize) != 5){
            *iEnd += addCoeficient(fileSize);
        }
        else{
            *iEnd += 5;
        }
    }

    return;
}
void iEnd_iStar_reducer(uint64_t* iStart, uint64_t* iEnd, uint64_t fileSize, uint16_t maxElements){
    uint64_t iEndPer5 = *iEnd%5;
    switch(iEndPer5){
        case 0:
            iEndPer5 = 5;
            break;
    }
    if(!((int64_t)*iStart-5 < 0) && (*iEnd-iEndPer5)-(*iStart-5) == maxElements){
        *iStart -= 5;

        switch(*iEnd%5){
            case 0:
                *iEnd -= 5;
                break;
            default:
                *iEnd -= addCoeficient(fileSize);
        }
    }

    return;
}


uint8_t coeficientCalcForJ(uint64_t i, uint64_t fileSize){
    if(i+5 > fileSize){
        return fileSize%5;
    }

    return 5;
}

uint8_t addCoeficient(uint64_t fileSize){
    uint8_t coeficient = fileSize % 5;

    if(coeficient == 0)
        return 5;
    else
        return coeficient;

    return coeficient;
}

// Thank you math and linear interpolation
void yLineatInterpolationForMinor(Rectangle* minor, Rectangle major, uint64_t iStart, uint64_t iEnd, uint64_t fileSize, uint16_t maxElements){
    float minN = (float)(0+maxElements)/2;
    float maxN = ((float)((int64_t)fileSize-maxElements)+(float)fileSize)/2;
    float n =  (float)(iStart+iEnd)/2;

    float maxY = calcP2(major).y+1-minor->height-3;
    float minY =  major.y+3;

    minor->y = (maxY-minY)*(n-minN)/(maxN-minN)+minY;

    return;
}
