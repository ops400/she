#include "deps/rlImGui.h"
#include "deps/imgui.h"
#include  "misc.hpp"
#include "editor.hpp"
#include "fileHandling.hpp"
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <raylib.h>
#include <string>
#define WWH 800
#define WN "SHE"

int main(int argc, char** argv){
    SetConfigFlags(FLAG_VSYNC_HINT | FLAG_WINDOW_RESIZABLE);
    InitWindow(WWH, WWH, WN);
    // SetTargetFPS(10);
    SetExitKey(KEY_NULL);
    rlImGuiSetup(true);

    ImGuiIO &io = ImGui::GetIO();
    (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    io.MouseDrawCursor = true;

    bool openModal = false, openSettingsWindow = false, openSaveErrorModal = false;
    bool startFileOpeningProcess = false;
    bool openHexEditor = false;

    char filePath[600];
    for(uint16_t i = 0; i < sizeof(filePath); i++) filePath[i] = 0;

    uint64_t fileSize = 0;

    static uint8_t* data;
    static uint16_t maxElements;
    maxElements = MAX_ELEMENTS;

    bool firstRun = true;

    bool debugInfoShow = false;

    ImGuiStyle* style = &ImGui::GetStyle();
    style->Colors[ImGuiCol_WindowBg] = customWindowBg;
    style->Colors[ImGuiCol_MenuBarBg] = customMenuBarBg;
    ImGuiWindowFlags mainWindowFlags = ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_MenuBar;
    // ImGuiWindowFlags mainWindowFlags = ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_MenuBar;

    while(!WindowShouldClose()){
        if(startFileOpeningProcess){
            fileSize = getFileSize(filePath);
            data = (uint8_t*)malloc(fileSize * sizeof(uint8_t));
            for(uint64_t i = 0; i < fileSize; i++) data[i] = 0;
            openFile(data, filePath, fileSize);
            startFileOpeningProcess = false;
            openHexEditor = true;
            firstRun = true;
        }
        BeginDrawing();
        rlImGuiBegin();
        ClearBackground(BLACK);

        ImGui::SetNextWindowSize(io.DisplaySize);
        ImGui::SetNextWindowPos((ImVec2){0.0f, 0.0f});
        if(ImGui::Begin("SHE", (bool*)true, mainWindowFlags)){
            if(ImGui::BeginMenuBar()){
                if(ImGui::BeginMenu("File")){
                    if(ImGui::MenuItem("Open")){
                        openModal = true;
                    }
                    if(ImGui::MenuItem("Save")){
                        if(!openHexEditor){
                            openSaveErrorModal = true;
                        }
                        else{
                            saveFile(data, filePath, fileSize);
                        }
                    }
                    ImGui::EndMenu();
                }

                if(ImGui::BeginMenu("Others")){
                    if(ImGui::MenuItem("Settings")){
                        openSettingsWindow = true;
                    }
                    ImGui::EndMenu();
                }

                if(ImGui::BeginMenu("Debug")){
                    ImGui::MenuItem("Show editor's debug info", NULL, &debugInfoShow);
                    if(debugInfoShow){
                        if(ImGui::MenuItem("open test3.bin")){
                            startFileOpeningProcess = true;
                            firstRun = true;
                            strcpy(filePath, "test3.bin");
                        }
                        if(ImGui::MenuItem("open random51.bin")){
                            startFileOpeningProcess = true;
                            firstRun = true;
                            strcpy(filePath, "random51.bin");
                        }
                        if(ImGui::MenuItem("open random52.bin")){
                            startFileOpeningProcess = true;
                            firstRun = true;
                            strcpy(filePath, "random52.bin");
                        }
                        if(ImGui::MenuItem("open random53.bin")){
                            startFileOpeningProcess = true;
                            firstRun = true;
                            strcpy(filePath, "random53.bin");
                        }
                        if(ImGui::MenuItem("open random54.bin")){
                            startFileOpeningProcess = true;
                            firstRun = true;
                            strcpy(filePath, "random54.bin");
                        }
                        if(ImGui::MenuItem("open random213.bin")){
                            startFileOpeningProcess = true;
                            firstRun = true;
                            strcpy(filePath, "random213.bin");
                        }
                        if(ImGui::MenuItem("open random255.bin")){
                            startFileOpeningProcess = true;
                            firstRun = true;
                            strcpy(filePath, "random255.bin");
                        }
                        if(ImGui::MenuItem("open random10k.bin")){
                            startFileOpeningProcess = true;
                            firstRun = true;
                            strcpy(filePath, "random10k.bin");
                        }
                        if(ImGui::MenuItem("open random.bin")){
                            startFileOpeningProcess = true;
                            firstRun = true;
                            strcpy(filePath, "random.bin");
                        }
                    }
                    ImGui::EndMenu();
                }
                ImGui::EndMenuBar();
            }

            if(openHexEditor) openEditor(data, style, fileSize, &firstRun, &maxElements, debugInfoShow);

            ImGui::ShowDemoWindow((bool*)true);

            if(openModal){
                ImGui::OpenPopup("open");
                if(ImGui::BeginPopupModal("open")){
                        ImGui::InputText("##FilePath", filePath, sizeof(filePath));
                        if(ImGui::Button("Open")){
                        openModal = false;
                        startFileOpeningProcess = true;
                        firstRun = true;
                        }
                        ImGui::SameLine();
                        if(ImGui::Button("Close")) openModal = false;
                    ImGui::EndPopup();
                }
            }

            if(openSaveErrorModal){
                ImGui::OpenPopup("Save Error!");
                if(ImGui::BeginPopupModal("Save Error!", &openSaveErrorModal)){
                       ImGui::TextColored(convColorImVec4(RED), "Error!");
                       ImGui::TextUnformatted("You dont have nothing open!");
                       if(ImGui::Button("Close")) openSaveErrorModal = false;
                    ImGui::EndPopup();
                }
            }
            ImGui::End();
        }

        if(openSettingsWindow){
            if(ImGui::Begin("Settings", &openSettingsWindow)){
                const uint16_t step5 = 5;
                ImGui::TextUnformatted("Welcome to settings!");
                ImGui::TextUnformatted("Please be cautious.");
                ImGui::NewLine();

                if(ImGui::BeginChild("max element's value")){
                    static bool showAsHex;
                    ImGui::Checkbox("Display \"max elements\" variable value as hexadecimal", &showAsHex);

                    if(showAsHex)
                        ImGui::Text("Current \"max elements\" variable value: 0x%hX", maxElements);
                    else
                        ImGui::Text("Current \"max elements\" variable value: %hu", maxElements);

                    ImGui::SameLine();
                    ImGui::TextDisabled("(?)");
                    if(ImGui::BeginItemTooltip()){
                        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
                        if(showAsHex)
                            ImGui::Text("\"max elements\"'s variable value will set how many 5 row bytes it will show.\nNow it will show 0x%hX rows of 5 bytes.\nKeep In mind that showing too many rows can make the program run really sluggish, the default (0x55) is the sweet spot that should run nice in all computers", maxElements/5);
                        else
                            ImGui::Text("\"max elements\"'s variable value will set how many 5 row bytes it will show.\nNow it will show %hu rows of 5 bytes.\nKeep In mind that showing too many rows can make the program run really sluggish, the default (85) is the sweet spot that should run nice in all computers", maxElements/5);

                        ImGui::TextColored(convColorImVec4(RED), "WARNING: It has to be a multiple of 5");
                        ImGui::PopTextWrapPos();
                        ImGui::EndTooltip();
                    }
                    ImGui::SetNextItemWidth(100.0f);
                    if(showAsHex)
                        ImGui::InputScalar("\"max elements\"'s variable value", ImGuiDataType_U16, &maxElements, &step5, NULL, "0x%hX");
                    else
                        ImGui::InputScalar("\"max elements\"'s variable value", ImGuiDataType_U16, &maxElements, &step5);

                    if(maxElements%5 == 0)
                        ImGui::TextColored(convColorImVec4(GREEN), "Ok!");
                    else
                        ImGui::TextColored(convColorImVec4(RED), "ERROR: It's not an multiple of 5!");
                    if(maxElements > MAX_ELEMENTS+15)
                        ImGui::TextColored(convColorImVec4(YELLOW), "WARNING: Value may be too big.");

                    ImGui::EndChild();
                }
                ImGui::End();
            }
        }

        rlImGuiEnd();
        EndDrawing();
    }

    rlImGuiShutdown();
    CloseWindow();
    return 0;
}
